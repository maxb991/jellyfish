<?php

namespace Jellyfish;

use NanoSoup\Zeus\Kernel as KernelBase;
use Jellyfish\ACF\Blocks\Content\ContentWithImage;


class Kernel extends KernelBase
{
    public function __construct()
    {
        parent::__construct();

        $this->registerClasses();
    }

    /**
     * @return array
     */
    public function registerClasses()
    {
        return [
          new ContentWithImage()
        ];
    }
}
