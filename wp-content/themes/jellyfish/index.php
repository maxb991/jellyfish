<?php

use Timber\Timber;
use Jellyfish\IndexController;

$page = IndexController::indexAction();

Timber::render($page['templates'], $page['context']);